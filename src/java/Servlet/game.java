/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author vdanoffr
 */
public class game extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(true);
        if (request.getParameter("JSESSIONID") != null) {
            Cookie userCookie = new Cookie("JSESSIONID", request.getParameter("JSESSIONID"));
            response.addCookie(userCookie);
        } else {
            String sessionId = session.getId();
            Cookie userCookie = new Cookie("JSESSIONID", sessionId);
            response.addCookie(userCookie);
        }
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Gamble !</title>");
            out.println("<link rel='stylesheet' type='text/css' href='style.css'>");
            out.println("</head>");
            out.println("<body>");
                
            //Connexion to MySQL
            Sql mySqlClass = new Sql("jdbc:mysql://localhost:3306/alt3", "alt3", "alt3");
            
            ResultSet res = null;
            Connection conn = null;
            Statement stmt=null;
            try{
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                conn = Sql.getConnexion();
                stmt = conn.createStatement();
            }catch(Exception e){out.print(e);}
            
            /*Get session
            Dans la sassion il y a :
            username : Nom d'utilisateur
            id : id utilisateur
            isAdmin : bolean si il doit être admin ou non
            avancement : page à afficher            
            */
            
            
            //TRAITEMENT DE L'AUTH DE LA PAGE CONNEXION AVEC DU SQL
            String username = request.getParameter("login");
            String password = request.getParameter("password");
            if(request.getParameter("submit_connexion") != null && username != null && password != null) {
                if(username.equals("") || password.equals("")) {
                    out.print("Veuillez remplir les champs");
                }
                else {
                    try {
                        PreparedStatement st = conn.prepareStatement("SELECT * from utilisateur WHERE username = ? and password = ?");
                        st.setString(1,username);
                        st.setString(2,password);
                        res = st.executeQuery();
                        res.next();
                        session.setAttribute("username", res.getString("username"));
                        session.setAttribute("id", res.getString("id_user"));
                        String isAdmin = res.getString("admin");
                        session.setAttribute("isAdmin", isAdmin);
                        if(isAdmin.equals("yes")) 
                            session.setAttribute("avancement", "p_admin");
                        else
                            session.setAttribute("avancement", "p_user");
                    }
                    catch(Exception e) {
                        out.println(e);
                    }
                } 
            } 
            
            //TRAITEMENT DECONNEXION
            if (request.getParameter("submit_deconnexion") != null && request.getParameter("submit_deconnexion").equals("Deconnexion")) {
                session.removeAttribute("username");
                session.removeAttribute("id");
                session.removeAttribute("isAdmin");
                session.removeAttribute("avancement");
                session.invalidate();
                response.setIntHeader("refresh", 1);
                out.println("Deconnexion faite !");
            }
            else if(request.getParameter("submit_game") != null) {
                String nbParties = request.getParameter("nbParty");
//                out.print("OK");
//                request.getRequestDispatcher("game.jsp").include(request, response);  
            }
            
            //TRAITEMENT LANCEMENT DU JEU
            //TODO et traitrer le request.getParameter("submit_game")!= null
            
            
            
            //REDIRECTION VERS LA BONNE PAGE
            if(session.getAttribute("avancement")==null || session.getAttribute("avancement").equals("")) {
                //Page de login a afficher (.jsp)
                request.getRequestDispatcher("connexion.jsp").include(request, response);
            }
            else if(session.getAttribute("avancement").equals("p_user")) {
                //Page user
                request.getRequestDispatcher("user.jsp").include(request, response);
            }
            else if(session.getAttribute("avancement").equals("p_admin")) {
                //Page admin
                request.getRequestDispatcher("admin.jsp").include(request, response);                
            }
            
            
            System.out.println("----- Debug ----");
            System.out.println("username = " + session.getAttribute("username"));
            System.out.println("id = " + session.getAttribute("id"));
            System.out.println("isAdmin = " + session.getAttribute("isAdmin"));
            System.out.println("avancement = " + session.getAttribute("avancement"));
            System.out.println(session.toString());
            

            
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
