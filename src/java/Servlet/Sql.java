/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Servlet;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vdanoffr
 */
public class Sql {
    static private Connection connexion;
    
    public Sql(String jdbc, String user, String mdp) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connexion = (Connection) DriverManager.getConnection(jdbc,user,mdp);
        }
        catch (Exception ex) {
            Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Connection getConnexion() {
        return connexion;
    }
    
        
    public static String resultToString(ResultSet rs) {
        if (rs == null) return "Pas de données";
        String res = "";
        
        ResultSetMetaData rsmd;
        try {
            rsmd = rs.getMetaData();
            int numOfCol = rsmd.getColumnCount();
            res = "<table border=1>"; //cellspacing=0 cellpadding=0

            res += "<thead>";
            for (int i = 1; i <= numOfCol; i++ ) {
                res += "<th>" + (rsmd.getColumnName(i).equals("tentative")?"Nombre de tentatives":rsmd.getColumnName(i)) + "</th>";
                // Do stuff with name
            }
            res += "</thead>";


            while (rs.next()) {
                System.out.println("Boucle lecture du select");
                res += "<tr>";
                for(int i = 1; i <= numOfCol; i++) {
                    res += "<td>" + rs.getString(i) + "</td>";
                }
                res += "</tr>";
            }

            res += "</table>";
            return res;
        } catch (SQLException ex) {
            Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    
    public static List<String> oneCollumnToList(ResultSet rs) throws SQLException {
        if (rs == null) return new ArrayList<String>();
        
        ArrayList<String> listResult = new ArrayList();
        while (rs.next()) {
            System.out.println("Ajout d'un nouveau rs : " + rs.getString(1));
            listResult.add(rs.getString(1));
        }
        return listResult;
    }
    
    public static ResultSet select(Connection conn,String sql) {
        try {
            Statement stmt=(Statement) conn.createStatement();
            PreparedStatement st=conn.prepareStatement(sql);
            System.out.println("Execution ok !");
            return st.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static void insertTry(Connection conn, int id_user, int nTry) { 
        // id_user pour l'id de l'utilisateur
        // nTry pour le nombre de tentative
        // table dans lequel on ajoute une ligne : historique
        System.out.println("Début insertTry");
        try {
            String ins="INSERT INTO historique(id_user,tentative) VALUES (?,?)";
            PreparedStatement st=conn.prepareStatement(ins);
            st.setInt(1,id_user);
            st.setInt(2,nTry);
            st.executeUpdate();
        }
        catch(Exception e) {
            Logger.getLogger(Sql.class.getName()).log(Level.SEVERE, null, e);
        }
        System.out.println("Début insertTry");
    }
    
}
