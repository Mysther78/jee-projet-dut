<%-- 
    Document   : historique
    Created on : 12 juin 2019, 11:30:24
    Author     : vdanoffr
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="Servlet.Sql"%>
<%@page import="com.mysql.jdbc.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%String title = (session.getAttribute("isAdmin")==null || !session.getAttribute("isAdmin").equals("yes"))?"Vous n'avez pas les privilèges nécessaires":"Historique de " + request.getParameter("username");%>
        
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%=title%></title>
    </head>
    <body>
        <h1><%=title%></h1>
        <%
            if(!(session.getAttribute("isAdmin")==null || !session.getAttribute("isAdmin").equals("yes"))) {
                Connection conn = Sql.getConnexion();
                //ResultSet resultat = Sql.select(conn, "select * from historique where id_user = " + session.getAttribute("id") + ";");
                ResultSet resultat = Sql.select(conn, "select id_user from utilisateur where username='" + request.getParameter("username") + "';");
                resultat.next();
                String right_id_user = resultat.getString("id_user");
                
                
                resultat = Sql.select(conn, "select (tentative) as 'Nombre de lancers', count(tentative) as 'Nombre de parties', count(tentative)/(select count(*) from historique where id_user=" + right_id_user + ") as 'Fréquence', (count(tentative)/(select count(*) from historique where id_user=" + right_id_user + "))*100 as 'Fréquence (%)' from historique where id_user=" + right_id_user + " group by tentative order by count(tentative) DESC;");
                String table = Sql.resultToString(resultat);
                out.println(table);
                
                
            }
            else{
                out.println("<p>Si vous pensez quand même avoir les privilèges, il s'agit sans doute d'un problème de cache ...<br>");
                out.println("Avez vous pensez à le vider ? (ou bien faire Ctrl + F5)");
            }
        %>
    </body>
</html>